<?php

return [
    "database" => [
        "host" => "127.0.0.1",
        "dbname" => "todos",
        "username" => "root",
        "password" => "118649qwe",
        "port" => "3306",
        "charset" => "utf8",
        "options" => [
            PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION
        ]
    ]
];
